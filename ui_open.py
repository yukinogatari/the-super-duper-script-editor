# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qt\ui\openmenu.ui'
#
# Created: Mon Mar 28 17:43:10 2016
#      by: PyQt4 UI code generator 4.11.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_OpenMenu(object):
    def setupUi(self, OpenMenu):
        OpenMenu.setObjectName(_fromUtf8("OpenMenu"))
        OpenMenu.resize(480, 324)
        OpenMenu.setMinimumSize(QtCore.QSize(480, 324))
        OpenMenu.setMaximumSize(QtCore.QSize(16777215, 16777215))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/monokuma-green.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        OpenMenu.setWindowIcon(icon)
        self.horizontalLayout = QtGui.QHBoxLayout(OpenMenu)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.treeFileList = QtGui.QTreeWidget(OpenMenu)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.treeFileList.sizePolicy().hasHeightForWidth())
        self.treeFileList.setSizePolicy(sizePolicy)
        self.treeFileList.setAlternatingRowColors(True)
        self.treeFileList.setIndentation(15)
        self.treeFileList.setAnimated(True)
        self.treeFileList.setObjectName(_fromUtf8("treeFileList"))
        self.treeFileList.header().setVisible(False)
        self.horizontalLayout.addWidget(self.treeFileList)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label = QtGui.QLabel(OpenMenu)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Meiryo UI"))
        font.setPointSize(11)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.lblChapter = QtGui.QLabel(OpenMenu)
        self.lblChapter.setIndent(10)
        self.lblChapter.setObjectName(_fromUtf8("lblChapter"))
        self.verticalLayout.addWidget(self.lblChapter)
        self.label_3 = QtGui.QLabel(OpenMenu)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Meiryo UI"))
        font.setPointSize(11)
        self.label_3.setFont(font)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.verticalLayout.addWidget(self.label_3)
        self.lblScene = QtGui.QLabel(OpenMenu)
        self.lblScene.setIndent(10)
        self.lblScene.setObjectName(_fromUtf8("lblScene"))
        self.verticalLayout.addWidget(self.lblScene)
        self.label_4 = QtGui.QLabel(OpenMenu)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Meiryo UI"))
        font.setPointSize(11)
        self.label_4.setFont(font)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.verticalLayout.addWidget(self.label_4)
        self.lblRoom = QtGui.QLabel(OpenMenu)
        self.lblRoom.setIndent(10)
        self.lblRoom.setObjectName(_fromUtf8("lblRoom"))
        self.verticalLayout.addWidget(self.lblRoom)
        self.label_6 = QtGui.QLabel(OpenMenu)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Meiryo UI"))
        font.setPointSize(11)
        self.label_6.setFont(font)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.verticalLayout.addWidget(self.label_6)
        self.lblMode = QtGui.QLabel(OpenMenu)
        self.lblMode.setIndent(10)
        self.lblMode.setObjectName(_fromUtf8("lblMode"))
        self.verticalLayout.addWidget(self.lblMode)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.label_2 = QtGui.QLabel(OpenMenu)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Meiryo UI"))
        font.setPointSize(11)
        self.label_2.setFont(font)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout.addWidget(self.label_2)
        self.txtSearch = QtGui.QLineEdit(OpenMenu)
        self.txtSearch.setObjectName(_fromUtf8("txtSearch"))
        self.verticalLayout.addWidget(self.txtSearch)
        self.buttonBox = QtGui.QDialogButtonBox(OpenMenu)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout.addWidget(self.buttonBox)
        self.horizontalLayout.addLayout(self.verticalLayout)

        self.retranslateUi(OpenMenu)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), OpenMenu.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), OpenMenu.reject)
        QtCore.QObject.connect(self.treeFileList, QtCore.SIGNAL(_fromUtf8("currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)")), OpenMenu.changeSelection)
        QtCore.QObject.connect(self.treeFileList, QtCore.SIGNAL(_fromUtf8("itemDoubleClicked(QTreeWidgetItem*,int)")), OpenMenu.doubleClicked)
        QtCore.QObject.connect(self.txtSearch, QtCore.SIGNAL(_fromUtf8("textEdited(QString)")), OpenMenu.findDirectory)
        QtCore.QMetaObject.connectSlotsByName(OpenMenu)

    def retranslateUi(self, OpenMenu):
        OpenMenu.setWindowTitle(_translate("OpenMenu", "Open", None))
        self.treeFileList.headerItem().setText(0, _translate("OpenMenu", "1", None))
        self.label.setText(_translate("OpenMenu", "Chapter", None))
        self.lblChapter.setText(_translate("OpenMenu", "TextLabel", None))
        self.label_3.setText(_translate("OpenMenu", "Scene", None))
        self.lblScene.setText(_translate("OpenMenu", "TextLabel", None))
        self.label_4.setText(_translate("OpenMenu", "Room", None))
        self.lblRoom.setText(_translate("OpenMenu", "TextLabel", None))
        self.label_6.setText(_translate("OpenMenu", "Mode", None))
        self.lblMode.setText(_translate("OpenMenu", "TextLabel", None))
        self.label_2.setText(_translate("OpenMenu", "Search", None))

import icons_rc
